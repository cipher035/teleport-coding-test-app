﻿using TeleportWebApp.DTOs;

namespace TeleportWebApp.Models
{
    public class UrbanAreaScore
    {
        public string Name { get; set; }
        public string Summary { get; set; }
        public decimal TeleportScore { get; set; }

        public UrbanAreaScore(UrbanAreaScoreResponse dto)
        {
            Summary = dto.Summary;
            TeleportScore = decimal.Round(dto.TeleportScore, 2);
        }
    }

    public class UrbanAreaDetail
    {
        public string Name { get; set; }
        public string UrbanAreaLink { get; set; }

        public UrbanAreaDetail(UrbanAreaDetailDTO dto)
        {
            Name = dto.Name;
            UrbanAreaLink = dto.UrbanAreaLink;
        }
    }
}
