﻿namespace TeleportWebApp.Models
{
    public class Settings
    {
        public Settings()
        {
            TeleportApiBaseUrl = "TeleportApiBaseUrl";
        }
        public string TeleportApiBaseUrl { get; set; }
    }
}
