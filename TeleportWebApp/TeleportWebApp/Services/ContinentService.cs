﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeleportWebApp.DTOs;
using TeleportWebApp.Interfaces;
using TeleportWebApp.Models;

namespace TeleportWebApp.Services
{
    public class ContinentService : IContinentService
    {
        private readonly IServiceConnection _serviceConnection;
        public ContinentService (IServiceConnection serviceConnection)
        {
            _serviceConnection = serviceConnection; 
        }

        public async Task<ContinentResponse> GetContinentsAsync()
        {
            var response = await _serviceConnection.GetAsync<ContinentResponse>("continents/");

            return response?.Continent?.ContinentList != null ? response : new ContinentResponse() { Continent = new ContinentDTO() { ContinentList = new List<ContinentDetailDTO>() } };
           
        }

        public async Task<UrbanAreaResponse> GetContinentUrbanAreasAsync(string continentUrl)
        {
            var urbanAreaRequest = $"{continentUrl}urban_areas/";

            var response = await _serviceConnection.GetAsync<UrbanAreaResponse>(urbanAreaRequest);

            return response?.UrbanArea?.UrbanAreaList != null ? response : new UrbanAreaResponse() { UrbanArea = new UrbanAreaDTO() { UrbanAreaList = new List<UrbanAreaDetailDTO>() } };          
        }

        public async Task<UrbanAreaScoreResponse> GetContinentUrbanAreasScoreAsync(string urbanAreaUrl)
        {
            var urbanAreaScoreRequest = $"{urbanAreaUrl}scores/";
            
            var response = await _serviceConnection.GetAsync<UrbanAreaScoreResponse>(urbanAreaScoreRequest);


            return response ?? new UrbanAreaScoreResponse();
        }

        /// <summary>
        ///  Gets all the urban areas with teleport city score 
        ///  which finally returns top N number of highest urban areas based on teleport city score.       
        /// </summary>
        /// <param name="urbanAreas"> Urban area detail list which has urban area name and urban area api url </param>
        /// <param name="number">Total number of top urban areas to return </param>
        /// <returns>Top N number of urban areas with highest teleport city score </returns>
        public async Task<List<UrbanAreaScore>> GetTopUrbanAreasByScoreAsync(List<UrbanAreaDetail> urbanAreas,int number)
        {
            var urbanAreaScoreList = new List<UrbanAreaScore>();
            foreach (var urbanArea in urbanAreas)
            {
                var scoreResponse = await GetContinentUrbanAreasScoreAsync(urbanArea.UrbanAreaLink);
                var urbanAreaScore = new UrbanAreaScore(scoreResponse);
                urbanAreaScore.Name = urbanArea.Name;

                urbanAreaScoreList.Add(urbanAreaScore);
            }

            var topUrbanAreas = urbanAreaScoreList.OrderByDescending(ua=> ua.TeleportScore).Take(number).ToList();

            return topUrbanAreas;
        }
    }
}
