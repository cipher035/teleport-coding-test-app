﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TeleportWebApp.DTOs
{
    [DataContract]
    public class ContinentResponse
    {
        [DataMember]
        [JsonProperty(PropertyName = "_links")]
        public ContinentDTO Continent { get; set; }
    }

    [DataContract]
    public class ContinentDTO
    {
        [DataMember]
        [JsonProperty(PropertyName = "continent:items")]
        public IEnumerable<ContinentDetailDTO> ContinentList { get; set; }
    }

    [DataContract]
    public class ContinentDetailDTO
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "href")]
        public string ContinentLink { get; set; }
    }
}
