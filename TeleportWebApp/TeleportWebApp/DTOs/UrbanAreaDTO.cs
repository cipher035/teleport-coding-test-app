﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TeleportWebApp.DTOs
{
    [DataContract]
    public class UrbanAreaResponse
    {
        [DataMember]
        [JsonProperty(PropertyName = "_links")]
        public UrbanAreaDTO UrbanArea { get; set; }
    }

    public class UrbanAreaDTO
    {
        [DataMember]
        [JsonProperty(PropertyName = "ua:items")]
        public List<UrbanAreaDetailDTO> UrbanAreaList { get; set; }
    }

    [DataContract]
    public class UrbanAreaDetailDTO
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "href")]
        public string UrbanAreaLink { get; set; }
    }

    [DataContract]
    public class UrbanAreaScoreResponse
    {       

        [DataMember]
        public string Summary { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "teleport_city_score")]
        public decimal TeleportScore { get; set; }
    }

   
}
