﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TeleportWebApp.Interfaces;
using TeleportWebApp.Models;

namespace TeleportWebApp.SeviceConnection
{
    public class ServiceConnection: IServiceConnection
    {
        private readonly Settings _settings;
        private readonly ILogger _serviceLogger;
        private readonly IHttpClientFactory _clientFactory;
        public ServiceConnection(IOptions<Settings> settings, IHttpClientFactory clientFactory, ILogger<ServiceConnection> serviceLogger)
        {
            _settings = settings.Value;
            _clientFactory = clientFactory;
            _serviceLogger = serviceLogger;
        }

        public async Task<T> GetAsync<T>(string requestUrl) where T:class
        {
            using (var client = _clientFactory.CreateClient())
            {
                //setup
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.teleport.v1+json"));

                HttpResponseMessage responseMessage = null;
                
                //send request
                try
                {
                    var request = requestUrl;
                    if (!requestUrl.Contains("api"))
                    {
                        var apiBaseUrl = _settings.TeleportApiBaseUrl;
                        request = $"{apiBaseUrl}/{requestUrl}";
                    }
                    responseMessage = await client.GetAsync(request); 

                }
                catch (Exception e)
                {                    
                    _serviceLogger.LogError(e.Message);
                    return null;
                }

                return await HandleResponseStatusAsync<T>(responseMessage);
            }
        }

        private async Task<T> HandleResponseStatusAsync<T>(HttpResponseMessage responseMessage) where T : class
        {
            return responseMessage.StatusCode == HttpStatusCode.OK ? await ConvertToResponseObjectAsync<T>(responseMessage) : null;         
        }

        private async Task<T> ConvertToResponseObjectAsync<T>(HttpResponseMessage responseMessage) where T : class
        {
            using (var stream = await responseMessage.Content.ReadAsStreamAsync())
            using (var streamReader = new StreamReader(stream))
            using (var reader = new JsonTextReader(streamReader))
            {   
                var serializer = new JsonSerializer();

                var response = serializer.Deserialize<T>(reader);
                return response;
            }
        }


    }
}
