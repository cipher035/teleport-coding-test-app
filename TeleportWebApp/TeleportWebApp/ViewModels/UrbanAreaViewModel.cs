﻿using System.Collections.Generic;
using TeleportWebApp.Models;

namespace TeleportWebApp.ViewModels
{
    public class UrbanAreaViewModel
    {
        public List<UrbanAreaScore> UrbanAreasScore { get; set; }

        public UrbanAreaViewModel(List<UrbanAreaScore> urbanAreaScoreList)
        {
            UrbanAreasScore = urbanAreaScoreList;
        }
    }

   
}
