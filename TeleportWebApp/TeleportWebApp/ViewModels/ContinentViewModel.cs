﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TeleportWebApp.DTOs;

namespace TeleportWebApp.ViewModels
{
    public class ContinentViewModel
    {
        [Display(Name="Select Continent")]
        public List<SelectListItem> Continent { get; set; }
        public ContinentViewModel(ContinentDTO continentDTO) // mapping from DTO to ViewModel 
        {            
            var continent = continentDTO.ContinentList.Select(cl => new ContinentDetailViewModel(cl)).ToList();

            Continent = new List<SelectListItem>(){ new SelectListItem(){ Text = string.Empty, Value = string.Empty }};
            Continent.AddRange(continent.Select(c => new SelectListItem() { Text = c.Name, Value = c.Link }).ToList());
        }
    }

    public class ContinentDetailViewModel
    {
        public string Name { get; set; }
        public string Link { get; set; }

        public ContinentDetailViewModel(ContinentDetailDTO continentDetailDTO) // mapping from DTO to ViewModel
        {
            Name = continentDetailDTO.Name;
            Link = continentDetailDTO.ContinentLink;
        }
    }

   

}
