﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TeleportWebApp.Interfaces;
using TeleportWebApp.Models;
using TeleportWebApp.ViewModels;

namespace TeleportWebApp.Controllers
{
    public class HomeController : Controller
    {        
        private readonly IContinentService _continentService;
        public HomeController(IContinentService continentService)
        {
            _continentService = continentService;
        }
        
        public async Task<IActionResult> Index()
        {
            var continentsResponse = await _continentService.GetContinentsAsync();           
                
            var vm = new ContinentViewModel(continentsResponse.Continent);

            return View(vm);
        }

        [HttpGet]
        public async Task<IActionResult> GetTopFiveUrbanAreas(string continentUrl)
        {
            UrbanAreaViewModel vm;
            if (string.IsNullOrEmpty(continentUrl))
            {
                var emptyUrbanAreaList = new List<UrbanAreaScore>();
            
                vm = new UrbanAreaViewModel(emptyUrbanAreaList);
            }
            else
            {
                var urbanAreasResponse = await _continentService.GetContinentUrbanAreasAsync(continentUrl);

                var urbanAreasList = urbanAreasResponse.UrbanArea.UrbanAreaList.Select(ua => new UrbanAreaDetail(ua)).ToList();

                var topFiveUrbanArea = await _continentService.GetTopUrbanAreasByScoreAsync(urbanAreasList, 5);
                
                vm = new UrbanAreaViewModel(topFiveUrbanArea);
            }    

            return PartialView("~/Views/Home/_UrbanAreaPartial.cshtml", vm);
        }

       
    }
}
