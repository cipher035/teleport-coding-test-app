﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeleportWebApp.DTOs;
using TeleportWebApp.Models;

namespace TeleportWebApp.Interfaces
{
    public interface IContinentService
    {
        Task<ContinentResponse> GetContinentsAsync();
        Task<UrbanAreaResponse> GetContinentUrbanAreasAsync(string continentUrl);
        Task<UrbanAreaScoreResponse> GetContinentUrbanAreasScoreAsync(string urbanAreaUrl);
        Task<List<UrbanAreaScore>> GetTopUrbanAreasByScoreAsync(List<UrbanAreaDetail> urbanAreas, int number);
    }
}
