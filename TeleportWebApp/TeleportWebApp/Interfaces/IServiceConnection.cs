﻿using System.Threading.Tasks;

namespace TeleportWebApp.Interfaces
{
    public interface IServiceConnection
    {
        Task<T> GetAsync<T>(string requestUrl) where T : class;
    }
}
