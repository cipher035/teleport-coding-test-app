using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using TeleportWebApp.DTOs;
using TeleportWebApp.Interfaces;
using TeleportWebApp.Services;

namespace TeleportWebAppUnitTests
{
    public class ContinentServiceTests
    {
        private Mock<IServiceConnection> _serviceConnection;
        private ContinentService _continentService;
        private DummyData _dummyData;

        [SetUp]
        public void Setup()
        {
            _serviceConnection = new Mock<IServiceConnection>();
            _dummyData = new DummyData();
        }

        [Test]
        public void GetContinent_Returns_Empty_Response_When_API_Returns_Null()
        {
            // Arrange
            _serviceConnection.Setup(sc => sc.GetAsync<ContinentResponse>(It.IsAny<string>())).ReturnsAsync(() => (ContinentResponse)null);
            _continentService = new ContinentService(_serviceConnection.Object);

            // Act
            var response = _continentService.GetContinentsAsync().Result;

            // Assert
            Assert.AreEqual(response.Continent.ContinentList.Count(), 0); 
        }

        [Test]
        public void GetContinentUrbanAreas_Sends_Request_With_Correct_Url_Appending_Urban_Areas_At_The_End()
        {
            // Arrange
            var continentUrl = "https://dummyapi-endpoint.com/continents/";
            var urbanAreasExpectedUrl = "https://dummyapi-endpoint.com/continents/urban_areas/";

            _serviceConnection.Setup(sc => sc.GetAsync<UrbanAreaResponse>(urbanAreasExpectedUrl)).ReturnsAsync(() => (UrbanAreaResponse)null);
            _continentService = new ContinentService(_serviceConnection.Object);
            
            // Act
            var response = _continentService.GetContinentUrbanAreasAsync(continentUrl).Result;

            // Assert
            _serviceConnection.Verify(sc => sc.GetAsync<UrbanAreaResponse>(urbanAreasExpectedUrl), Times.Once);
        }

        [Test]
        public void GetTopFiveUrbanAreas_Returns_Top_Five_Urban_Areas_With_Highest_Teleport_Score()
        {
            // Arrange
            var urbanAreas = _dummyData.GetUrbanAreas();   
            var expectedHighestTeleportScore = new List<decimal>
            {
                85.50m,
                65m,
                40m,
                35m,
                30.50m
            };
            var expectedNumber = 5; // number of top urban areas expected to return

            _serviceConnection.Setup(sc => sc.GetAsync<UrbanAreaScoreResponse>(It.IsAny<string>())).ReturnsAsync(()=> _dummyData.UrbanAreaScoreList.Dequeue());
            _continentService = new ContinentService(_serviceConnection.Object);

            // Act
            var response = _continentService.GetTopUrbanAreasByScoreAsync(urbanAreas, expectedNumber).Result;

            // Assert
            Assert.AreEqual(expectedNumber, response.Count());
            Assert.AreEqual(expectedHighestTeleportScore[0], response.First().TeleportScore);
            Assert.AreEqual(expectedHighestTeleportScore[1], response.Skip(1).First().TeleportScore);
            Assert.AreEqual(expectedHighestTeleportScore[2], response.Skip(2).First().TeleportScore);
            Assert.AreEqual(expectedHighestTeleportScore[3], response.Skip(3).First().TeleportScore);
            Assert.AreEqual(expectedHighestTeleportScore[4], response.Last().TeleportScore);

        }
    }
}