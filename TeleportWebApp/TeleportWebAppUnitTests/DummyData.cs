﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeleportWebApp.DTOs;
using TeleportWebApp.Models;

namespace TeleportWebAppUnitTests
{
    public class DummyData
    {
        public Queue<UrbanAreaScoreResponse> UrbanAreaScoreList { get;}

        public DummyData()
        {
            UrbanAreaScoreList = new Queue<UrbanAreaScoreResponse>();

            UrbanAreaScoreList.Enqueue(new UrbanAreaScoreResponse() { Summary = "dummy1", TeleportScore = 25.50m });
            UrbanAreaScoreList.Enqueue(new UrbanAreaScoreResponse() { Summary = "dummy2", TeleportScore = 35m });
            UrbanAreaScoreList.Enqueue(new UrbanAreaScoreResponse() { Summary = "dummy3", TeleportScore = 40m });
            UrbanAreaScoreList.Enqueue(new UrbanAreaScoreResponse() { Summary = "dummy4", TeleportScore = 20.50m });
            UrbanAreaScoreList.Enqueue(new UrbanAreaScoreResponse() { Summary = "dummy5", TeleportScore = 85.50m });
            UrbanAreaScoreList.Enqueue(new UrbanAreaScoreResponse() { Summary = "dummy6", TeleportScore = 65m });
            UrbanAreaScoreList.Enqueue(new UrbanAreaScoreResponse() { Summary = "dummy7", TeleportScore = 10m });
            UrbanAreaScoreList.Enqueue(new UrbanAreaScoreResponse() { Summary = "dummy8", TeleportScore = 30.50m });                         
        }

        private List<UrbanAreaDetailDTO> GetUrbanAreasDetail()
        {
            return new List<UrbanAreaDetailDTO>()
            {
                new UrbanAreaDetailDTO(){ Name="dummy_name_1", UrbanAreaLink="dummy_link_1"},
                new UrbanAreaDetailDTO(){ Name="dummy_name_2", UrbanAreaLink="dummy_link_2"},
                new UrbanAreaDetailDTO(){ Name="dummy_name_3", UrbanAreaLink="dummy_link_3"},
                new UrbanAreaDetailDTO(){ Name="dummy_name_4", UrbanAreaLink="dummy_link_4"},
                new UrbanAreaDetailDTO(){ Name="dummy_name_5", UrbanAreaLink="dummy_link_5"},
                new UrbanAreaDetailDTO(){ Name="dummy_name_6", UrbanAreaLink="dummy_link_6"},
                new UrbanAreaDetailDTO(){ Name="dummy_name_7", UrbanAreaLink="dummy_link_7"},
                new UrbanAreaDetailDTO(){ Name="dummy_name_8", UrbanAreaLink="dummy_link_8"}
            };
        }

        public List<UrbanAreaDetail> GetUrbanAreas()
        {
            return GetUrbanAreasDetail().Select(ua => new UrbanAreaDetail(ua)).ToList();
        }

    }
}
