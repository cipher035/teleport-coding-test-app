# Simple Web App Using Teleport Web Api

The solution contains a .NET core MVC web app, TeleportWebApp and TeleportWebApp Unit tests.

Web app has following folders:

* ServiceConnection - It contains a service class which uses HttpClientFactory and has generic Get method which sends http request to teleport web api and deserialize response to DTO's

* DTOs - It has data transfer objects used to hold the deserialized response back from api

* Interfaces - It has interfaces for the services

* Models -It holds the POCO models. Settings model is a Configuration class to hold configuration properties for strongly typing properties in appsettings.json

* Services -It holds a Continent service class which has ServiceConnection instance injected and it has methods to get continent list, urban areas list and N number of top urban areas based on teleport score 

* ViewModels - It has classes to hold and display view data. DTO's are mapped to view models in view model's constructor

* Views- It has views associated with controllers. Home/Index.cshtml displays the dropdown continent list. Home/_UrbanAreaPartial.cshtml , partial view, is returned as a result of ajax request initiated by selecting continent dropdown, which holds the results of top urban areas in a table

## TeleportWebApp Unit Tests

* Has unit tests for Continent service methods: GetContinentsAsync, GetContinentUrbanAreasAsync, GetTopUrbanAreasByScoreAsync

* Test that confirms the GetContinentsAsync method returns empty response whenever api response is null (may be due to some error at api end )

* Test that confirms the GetContinentUrbanAreasAsync method builds up correct urban areas api url by appending "urban_areas/" at the end ( have a look inside GetContinentUrbanAreasAsync method. Remove "urban_areas/" and see the test fail, adding back will pass the test)

* Test that confirms the GetTopUrbanAreasByScoreAsync method returns N number of highest Teleport Scored Urban Areas




